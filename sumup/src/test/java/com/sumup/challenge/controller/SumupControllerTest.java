package com.sumup.challenge.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class SumupControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@InjectMocks
	private SumupController controllerMock;


	@Test
	public void testGetTasks() throws Exception {
		String body = "{\"tasks\": [ {\"name\":\"task-1\",\"command\":\"touch /tmp/file1\"}]}";
		mockMvc.perform(post("/sumup/task")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content(body))
		        .andExpect(status().isOk());
	}
}
