package com.sumup.challenge.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import com.sumup.challenge.request.Task;
import com.sumup.challenge.request.TaskRequest;
import com.sumup.challenge.response.TaskResponse;
import com.sumup.challenge.service.impl.SumupServiceImpl;

public class SumupServiceTest {

	@Mock
	private SumupServiceImpl serviceMock;

	@BeforeEach
	public void setUp() {
		serviceMock = new SumupServiceImpl();
	}

	@Test
	public void testGetTasks() {
		// GIVEN
		Task task = createTask();

		// WHEN
		Set<TaskResponse> expected = serviceMock.getTasks(task);

		// THEN
		assertNotNull(expected);
		assertNotNull(expected.stream().findFirst().get().getName());
		assertNotNull(expected.stream().findFirst().get().getCommand());
	}

	private Task createTask() {
		Set<String> requires = new HashSet<>();
		requires.add("task-1");
		requires.add("task-2");

		TaskRequest req = new TaskRequest();
		req.setName("nameMock");
		req.setCommand("commandMock");
		req.setRequires(requires);

		Set<TaskRequest> taskRequests = new HashSet<>();
		taskRequests.add(req);

		Task task = new Task();
		task.setTasks(taskRequests);

		return task;
	}
}
