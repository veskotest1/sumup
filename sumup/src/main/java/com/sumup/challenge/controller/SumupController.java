package com.sumup.challenge.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sumup.challenge.request.Task;
import com.sumup.challenge.response.TaskResponse;
import com.sumup.challenge.service.SumupService;

@Controller
@RequestMapping("/sumup")
public class SumupController {

	@Autowired
	private SumupService service;

	@PostMapping("/task")
	@ResponseBody
	public ResponseEntity<Set<TaskResponse>> getTasks(@RequestBody Task task) {
		return ResponseEntity.ok(service.getTasks(task));
	}
}
