package com.sumup.challenge.request;

import java.util.Set;

public class Task {
	private Set<TaskRequest> tasks;

	public Set<TaskRequest> getTasks() {
		return tasks;
	}

	public void setTasks(Set<TaskRequest> tasks) {
		this.tasks = tasks;
	}

}
