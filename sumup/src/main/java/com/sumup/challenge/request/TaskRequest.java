package com.sumup.challenge.request;

import java.util.Set;

public class TaskRequest {
	private String name;
	private String command;
	private Set<String> requires;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Set<String> getRequires() {
		return requires;
	}

	public void setRequires(Set<String> requires) {
		this.requires = requires;
	}

}
