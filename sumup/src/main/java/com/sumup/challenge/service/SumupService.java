package com.sumup.challenge.service;

import java.util.Set;

import com.sumup.challenge.request.Task;
import com.sumup.challenge.response.TaskResponse;

public interface SumupService {

	public Set<TaskResponse> getTasks(Task request);

}
