package com.sumup.challenge.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.sumup.challenge.request.Task;
import com.sumup.challenge.request.TaskRequest;
import com.sumup.challenge.response.TaskResponse;
import com.sumup.challenge.service.SumupService;

@Service
public class SumupServiceImpl implements SumupService {

	@Override
	public Set<TaskResponse> getTasks(Task task) {
		Set<TaskResponse> responses = new HashSet<>();

		for (TaskRequest taskRequest : task.getTasks()) {
			TaskResponse response = new TaskResponse();
			response.setName(taskRequest.getName());
			response.setCommand(taskRequest.getCommand());
			responses.add(response);
		}

		return responses;
	}

}
